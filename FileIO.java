import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.util.Scanner;

/**
* This is a class containing All CRUD methods to do fileIO 
* operations using JSON.
* @author Ziya
*
*/

public class FileIO {
	@SuppressWarnings({ "resource", "unused" })
	public static void main(String[] args) throws IOException {

		Scanner sc = new Scanner(System.in);
		System.out.println("THESE ARE FILE IO OPERATIONS");
		System.out.println("----------------------------");
		System.out.println("1.CREATE A NEW JSON FILE");
		System.out.println("2.READING THE DATA IN THE JSON FILE");
		System.out.println("3.WRITE or ADDING THE DATA IN THE JSON FILE");
	    System.out.println("4.UPDATING THE DATA IN THE JSON FILE");
		System.out.println("5.DELETE  DATA FROM THE JSON FILE");
		System.out.println("6.DELETE THE JSON FILE");
		System.out.println("\n");
		System.out.println("***** Enter your choice *****");
		int ch = sc.nextInt();
		switch (ch) {
		case 1:

			// creating a new Json file

			FileCreation fc = new FileCreation();
			try {
				fc.createFile("Ziya");
			}catch(FileAlreadyExistsException e) {
				e.printStackTrace();
			}
			System.out.println("JSON file is created successfully.....");
			break;
			
		case 2:

			// Reading the data in the Json file

			JsonFileReader fr = new JsonFileReader();
			//FileAddition fr=new FileAddition();
			fr.read();
			break;
			
			
		case 3:

			// writing or adding data to the JSON fie

			JsonFileWriter add = new JsonFileWriter();
			add.addNewFile();
			break;
		case 6:

			// creating a new Json file

			FileDeletion fd = new FileDeletion();
			fd.deleteFile("Ziya");
			System.out.println("JSON file is deleted successfully.....");
			break;
			

		}
	}
}
