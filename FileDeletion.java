import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileDeletion {
	/**
	 * Created a helper method to delete the file 
	 * @param fileName
	 * @return 
	 */

	public boolean deleteFile(String fileName) {
		Path path = Paths.get(fileName);
		try {
			if (Files.deleteIfExists(path)) {
				System.out.println("created File is deleted ");
			} else {
				System.out.println("failed to delete the file");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}
		
	}

