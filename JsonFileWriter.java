import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class JsonFileWriter {
	
	/**
	 * Created a helper method to write in the file 
	 * @param fileName
	 */
	@SuppressWarnings("unchecked")
	public void addNewFile() throws IOException {
		File file = new File("Ziya.dat");
		FileWriter fileWriter = new FileWriter(file);

		JSONObject User1 = new JSONObject();

		User1.put("Name", "Ziya");
		User1.put("Age", "21");
		User1.put("Gender", "Female");
		User1.put("Height", "167 cm");
		User1.put("Weight", "56 kgs");
		User1.put("Designation", "Software Trainee");
		User1.put("Salary", "25000");
		User1.put("Location", "Guntur");
		JSONArray listOfhobbies1 = new JSONArray();
		listOfhobbies1.add("Surfing");
		listOfhobbies1.add("Sleeping");
		listOfhobbies1.add("Travelling");
		User1.put("Hobby", listOfhobbies1);
		JSONArray listOffooditems1 = new JSONArray();
		listOffooditems1.add("Falooda");
		listOffooditems1.add("Ice Cream");
		listOffooditems1.add("Badam Keer");
		User1.put("Food Item", listOffooditems1);
		JSONArray listOfplaces1 = new JSONArray();
		listOfplaces1.add("Kashmir");
		listOfplaces1.add("Ooty");
		listOfplaces1.add("Agra");
		User1.put("Place", listOfplaces1);

		JSONObject userObject1 = new JSONObject();
		userObject1.put("First User : ", User1);

		// updating the json file by creation new user

		JSONObject User2 = new JSONObject();

		User2.put("Name", "Sonu");
		User2.put("Age", "23");
		User2.put("Gender", "Male");
		User2.put("Height", "178cm");
		User2.put("Weight", "54kgs");
		User2.put("Designation", "Software Engineer");
		User2.put("Salary", "35000");
		User2.put("Location", "Hyderabad");
		JSONArray listOfhobbies2 = new JSONArray();
		listOfhobbies2.add("Exploring");
		listOfhobbies2.add("Sleeping");
		listOfhobbies2.add("Cricket");
		User2.put("Hobby", listOfhobbies2);
		JSONArray listOffooditems2 = new JSONArray();
		listOffooditems2.add("Chi Biriyani");
		listOffooditems2.add("Noodles");
		listOffooditems2.add("Chocolate");
		User2.put("Food Item", listOffooditems2);
		JSONArray listOfplaces2 = new JSONArray();
		listOfplaces2.add("Saudi Arabia");
		listOfplaces2.add("Vizag");
		listOfplaces2.add("Mumbai");
		User2.put("Place", listOfplaces2);

		JSONObject userObject2 = new JSONObject();
		userObject2.put("Second User : ", User2);
		
		JSONObject User3 = new JSONObject();

		User3.put("Name", "Monu");
		User3.put("Age", "19");
		User3.put("Gender", "Male");
		User3.put("Height", "170cm");
		User3.put("Weight", "60kgs");
		User3.put("Designation", "Civil Engineer");
		User3.put("Salary", "30000");
		User3.put("Location", "Vizag");
		JSONArray listOfhobbies3 = new JSONArray();
		listOfhobbies3.add("Gaming");
		listOfhobbies3.add("Carroms");
		listOfhobbies3.add("Bowling");
		User3.put("Hobby", listOfhobbies3);
		JSONArray listOffooditems3 = new JSONArray();
		listOffooditems3.add("Mutton Biriyani");
		listOffooditems3.add("Manchuriya");
		listOffooditems3.add("Pastery");
		User3.put("Food Item", listOffooditems3);
		JSONArray listOfplaces3 = new JSONArray();
		listOfplaces3.add("Goa");
		listOfplaces3.add("Hyderabad");
		listOfplaces3.add("Kerala");
		User3.put("Place", listOfplaces3);

		JSONObject userObject3 = new JSONObject();
		userObject2.put("Third User : ", User3);

		// Add All the created users to list

		JSONArray userList = new JSONArray();
		userList.add(userObject1);
		userList.add(userObject2);
		userList.add(userObject3);

		// Writing to the JSON file

		fileWriter.write(userList.toJSONString());
		fileWriter.flush();
		fileWriter.close();
		System.out.println("Data Added to the file Successfully...");
		System.out.println("\n");
		System.out.println(User1);
		System.out.println(User2);
		System.out.println(User3);

	}
	public void read() throws FileNotFoundException {
		File file = new File("Ziya.dat");
		Scanner reader=new Scanner(file);
		while(reader.hasNextLine()) {
			String filedata=reader.nextLine();
			System.out.println(filedata);
		}
		reader.close();
	}
}
