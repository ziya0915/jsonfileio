import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class JsonFileReader {
	
	/**
	 * Created a helper method to read the data in the file 
	 * @param fileName
	 */

	public void read() throws FileNotFoundException {
		File file = new File("Ziya.dat");
		Scanner reader = new Scanner(file);
		while (reader.hasNextLine()) {
			String filedata = reader.nextLine();
			System.out.println(filedata);
		}
		reader.close();
	}
}
