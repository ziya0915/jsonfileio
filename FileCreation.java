import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileCreation {

	// create a json file

	public boolean createFile(String fileName) throws FileAlreadyExistsException{
		Path path = Paths.get(fileName);
		try {
			// creates file at specified location
			Files.createFile(path);
			System.out.println("File Created :");
		} catch (FileAlreadyExistsException fae) {
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}
		
	}
